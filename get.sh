# bin/bash

mkdir /var/www/moodle; cd /var/www; wget -O r.tar.bz2 https://bitbucket.org/bitsrv/moodle/get/MOODLE_30_STABLE.tar.bz2; tar jxf r.tar.bz2 -C moodle/; rm r.tar.*; cd /var/www/moodle/; mv $(ls)/* ./;rm -r bitsrv-moodle-*;
cd /var/www/moodle/auth; wget -O r_casdb.tar.bz2 https://bitbucket.org/bitsrv/moodle-auth-casdb/get/master.tar.bz2; tar jxf r_casdb.tar.bz2 -C casdb/; rm r_casdb.tar.*; cd /var/www/moodle/auth/casdb/; mv $(ls)/* ./;rm -r bitsrv-moodle-*;
cd /var/www/moodle/blocks; wget -O r_lac.tar.bz2 https://bitbucket.org/bitsrv/moodle-block-programming_latest_ac/get/master.tar.bz2; tar jxf r_lac.tar.bz2 -C programming_latest_ac/; rm r_lac.tar.*; cd /var/www/moodle/blocks/programming_latest_ac/; mv $(ls)/* ./;rm -r bitsrv-moodle-*;
cd /var/www/moodle/blocks; wget -O r_ps.tar.bz2 https://bitbucket.org/bitsrv/moodle-block-programming_standing/get/master.tar.bz2; tar jxf r_ps.tar.bz2 -C programming_standing/; rm r_ps.tar.*; cd /var/www/moodle/blocks/programming_standing/; mv $(ls)/* ./;rm -r bitsrv-moodle-*;
cd /var/www/moodle/blocks; wget -O r_pp.tar.bz2 https://bitbucket.org/bitsrv/moodle-block-programming_printer/get/master.tar.bz2; tar jxf r_pp.tar.bz2 -C programming_printer/; rm r_pp.tar.*; cd /var/www/moodle/blocks/programming_printer/; mv $(ls)/* ./;rm -r bitsrv-moodle-*;
cd /var/www/moodle/message/output; wget -O r_wn.tar.bz2 https://bitbucket.org/bitsrv/moodle-message-wechatnotifier/get/master.tar.bz2; tar jxf r_wn.tar.bz2 -C wechatnotifier/; rm r_wn.tar.*; cd /var/www/moodle/message/output/wechatnotifier/; mv $(ls)/* ./;rm -r bitsrv-moodle-*;
# cd /var/www/moodle/mod/; wget -O r_pr.tar.bz2 https://bitbucket.org/bitsrv/moodle-mod-programming/get/master.tar.bz2; tar jxf r_pr.tar.bz2 -C programming/; rm r_pr.tar.*; cd /var/www/moodle/mod/programming/; mv $(ls)/* ./;rm -r bitsrv-moodle-*;
cd /var/www/moodle/theme/; wget -O r_lambd.tar.bz2 https://bitbucket.org/bitsrv/moodle-theme-lambda/get/master.tar.bz2; tar jxf r_lambd.tar.bz2 -C lambda/; rm r_lambd.tar.*; cd /var/www/moodle/theme/lambda/; mv $(ls)/* ./;rm -r bitsrv-moodle-*;

